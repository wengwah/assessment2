##Assessment 2 Readme


###Last Commit @ 5.30pm

  * Sql statement for edit product works on workbench

####Unresolved issues
  * Sort and Filter functions do not work
  * Could not traverse to edit page (Page2) with parametrized routing
  * No UPC12 barcode image displayed (Low priority)

###Third Commit @ 3pm

  * Successfully completed and tested addProduct function
  * Sort and filter bars do not work

###Second Commit @ 1pm

  * Successfully populated data fields onto overview
  * Some issues with refactoring requires attention

###First Commit @ 11am

  * Scafolding complete
  * To test $state

