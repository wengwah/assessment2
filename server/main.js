// load the node libraries
const express = require("express");
const path = require("path");
const bodyParser = require("body-parser");
const mysql = require("mysql");
const q = require("q");

// Config database
const sampledb = mysql.createPool({
  host: "localhost", port: 3306,
  user: "fred", password: "fred",
  database: "grocerdb",
  connectionLimit: 5
});

// Create generalized query
const mkQuery = function(sql, pool) {
  return (function(/* some params */) {
    //Create a defer because mysql query will be
    //async
    const defer = q.defer();
    //
    //Collect the arguments
    var params = [];
    for (var i in arguments)
      params.push(arguments[i]);

    pool.getConnection(function(err, conn) {
      //Check if we have gotten a connection
      if (err) {
        //If there are any errors, reject the promise
        defer.reject(err);
        return;
      }

      //Make the query
      conn.query(sql, params, function(err, result) {
        if (err)
          defer.reject(err);
        else
          defer.resolve(result);
        conn.release();
      });
    });
    //Return the promise that is associated with the defer 
    return (defer.promise);
  });
}

// SQL Statements
const SELECT_ALL_PRODUCTS = "select * from grocery_list";
const SELECT_20_PRODUCTS = "select * from grocery_list limit 20";
const SELECT_PRODUCTS_BY_UPC12 = 
  "select * from grocery_list where UPC12 = ?";
const ADD_PRODUCT = 
  " insert into grocery_list(id, upc12, brand, name) values(?, ?, ?, ?)";
const UPDATE_PRODUCT = 
  "update grocery_list set brand=?, name=? where id = ";


// Javascript functions to make sql calls
const displayAllProducts = mkQuery(SELECT_ALL_PRODUCTS, sampledb);
const display20Products = mkQuery(SELECT_20_PRODUCTS, sampledb);
const displayProduct = mkQuery(SELECT_PRODUCTS_BY_UPC12, sampledb);

// create instance of express
const app = express();

//Config the routes

// Displays 20 products on Overview
app.get("/overview", function (req, res) {
  display20Products()
    .then(function(result) {
      res.status(200);
      res.type("application/json");
      res.json(result); 

    }).catch(function(err) {
      res.status(500);
      res.type("application/json");
      res.json(err);
    });
});

// 
app.get("/add", function(req, res) {

    var id = req.query.upc12.substring(6,12);

    console.info("id = %s", id);
    console.info("upc12 = %s", req.query.upc12);
    console.info("brand = %s", req.query.brand);
    console.info("name = %s", req.query.name);

    sampledb.getConnection(function(err, conn) {
        //Connection pool error
        if (err) {
            console.error(">>>> error: ", err);
            res.status(500);
            res.end(JSON.stringify(err));
            return;
        }
        //We have a connection
        conn.query(ADD_PRODUCT
            , [ id, req.query.upc12, req.query.brand, req.query.name ]
            , function(err, result) {
                if (err) {
                    console.error(">>>> insert: ", err);
                    res.status(500);
                    res.end(JSON.stringify(err));
                    conn.release();
                    return;
                }
            });
        
        conn.release();
        res.status(200);
        res.type("text/html");
        res.send("<h2>Registered</h2>");
    })

});



// Sanitize req and res data where possible
app.use(bodyParser.urlencoded( { extended: false }));
app.use(bodyParser.json());

// Define static paths
const CLIENT_FOLDER = path.join(__dirname, "../client");
app.use("/", express.static(CLIENT_FOLDER));

const BOWER_COMPONENTS = path.join(__dirname, "../bower_components");
app.use("/libs", express.static(BOWER_COMPONENTS));


var port = process.env.PORT || 3000;

app.listen(port, function() {
  console.log("Web app started at port %d", port);
});

// Make this app available for public eg. testing
module.exports = app;