(function() {
  angular
    .module("GrocerApp")
    .service("EditSvc", EditSvc);

  EditSvc.$inject = ["$http", "$q"];

  function EditSvc($http, $q) {
    var editSvc = this; // overviewSvc

    // Instantiating functions
    editSvc.editProduct = editProduct;

    // SQL statements here

    // not used
    function editProduct() {
      var defer = $q.defer();

      $http.get( )

      .then(function (result) {

        defer.resolve(result.data);
      })
      .catch (function (error) {
        defer.reject(error);
      })
      return (defer.promise);
    
    }

  }

}) ();