(function() {
  angular.module("GrocerApp").service("AddSvc", AddSvc);

  AddSvc.$inject = ["$http", "$q"];

  function AddSvc($http, $q) {
    var addSvc = this; // overviewSvc

    // Instantiating functions
    addSvc.addProduct = addProduct;

    // SQL statements here

    // not used
    function addProduct() {
      var defer = $q.defer();

      $http.get( )

      .then(function (result) {

        defer.resolve(result.data);
      })
      .catch (function (error) {
        defer.reject(error);
      })
      return (defer.promise);
    
    }

  }

}) ();