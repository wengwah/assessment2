// Overview Controller

(function () {
  "use strict";
  angular
    .module("GrocerApp")
    .controller("OverviewCtrl", OverviewCtrl);

  OverviewCtrl.$inject = ["$state", "OverviewSvc"];


  function OverviewCtrl($state, OverviewSvc) {
    var overviewCtrl = this; // vm

    overviewCtrl.products = [];
    overviewCtrl.product = {};

    OverviewSvc.display20Products()
      .then(function (result) {
        console.log(overviewCtrl.products);
        console.log(result);
        console.log(result[1].name);
        overviewCtrl.products = result;
        
      })
      .catch(function (error) {
        console.error("error = ", error);
      });

    // vm.sortBy = function(propertyName){
    //         vm.reverse = (vm.propertyName === propertyName) ? !vm.reverse: false;
    //         vm.propertyName = propertyName;
    //     }

  }

  // GrocerApp.config(GrocerConfig);
  // GrocerApp.service("OverviewSvc", OverviewSvc);


}) ();