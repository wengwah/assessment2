(function () {
  "use strict";
  angular
    .module("GrocerApp")
    .controller("AddCtrl", AddCtrl);

  AddCtrl.$inject = ["$state", "AddSvc"];

  function AddCtrl($state, AddSvc) {
    var addCtrl = this; // vm

    addCtrl.message = "Product Added!";


  }

  // GrocerApp.config(GrocerConfig);
  // GrocerApp.service("AddSvc", AddSvc);


}) ();