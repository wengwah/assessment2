(function () {
    "use strict";
    angular
      .module("GrocerApp")
      .config(GrocerConfig);

    GrocerConfig.$inject = ["$stateProvider", "$urlRouterProvider"];
    
    function GrocerConfig($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state("overview", {
                url: "/overview",
                templateUrl: "/views/overview.html",
                controller: "OverviewCtrl as overviewCtrl"
            })
            .state("edit", {
                url: "/edit", // to change to param route edit/:id
                templateUrl: "/views/edit.html", 
                controller: "EditCtrl as editCtrl" 
            })
            .state("add", {
                url: "/add", 
                templateUrl: "/views/add.html", 
                controller: "AddCtrl as addCtrl" 
            })

        $urlRouterProvider.otherwise("/overview");

    } 
    
})();